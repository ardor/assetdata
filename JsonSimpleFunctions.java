import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.SortedSet;
import java.util.TreeSet;

public class JsonSimpleFunctions {
    public static String getString(JSONObject jsonObject, String key, String defaultValue) {
        String result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (String) jsonObject.get(key);
            }
        }

        return result;
    }

    public static boolean getBoolean(JSONObject jsonObject, String key, boolean defaultValue) {
        boolean result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (boolean) jsonObject.get(key);
            }
        }

        return result;
    }

    public static int getInt(JSONObject jsonObject, String key, int defaultValue) {
        int result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (int) jsonObject.get(key);
            }
        }

        return result;
    }

    public static long getLong(JSONObject jsonObject, String key, long defaultValue) {
        long result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = (long) jsonObject.get(key);
            }
        }

        return result;
    }

    public static long getLongFromStringUnsigned(JSONObject jsonObject, String key, long defaultValue) {
        long result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = Long.parseUnsignedLong((String) jsonObject.get(key));
            }
        }

        return result;
    }

    public static SortedSet<Long> setLongFromJsonStringArray(JSONArray jsonStringArray) {

        int jsonArraySize = jsonStringArray.size();

        if( jsonArraySize == 0)
            return null;

        SortedSet<Long> list = new TreeSet<>();

        for(int i = 0; i < jsonArraySize; i++) {
            list.add(Long.parseUnsignedLong(jsonStringArray.get(i).toString()));
        }

        return list;
    }

    public static byte[] getBytesFromHexString(JSONObject jsonObject, String key, byte[] defaultValue) {
        byte[] result = defaultValue;

        if(jsonObject != null) {
            if(jsonObject.containsKey(key)) {
                result = bytesFromHexString((String) jsonObject.get(key));
            }
        }

        return result;
    }

    public static byte[] bytesFromHexString(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];

        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
        }

        return data;
    }
}
