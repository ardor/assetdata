import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import java.math.BigInteger;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

class AssetData {
    private static final HashMap<String, String> applicationResult = new HashMap<>();

    static private MessageDigest messageDigest;

    static {
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static ArdorApi ardorApi;
    private static NxtCryptography nxtCryptography;

    private static int indexForInterleave = 0;

    private enum HeaderType {message, asset, accountProperty};

    private static class ApplicationConfiguration {
        private final boolean useChainInterleaveFormatForPrunable = true;

        private final int nonceSize = 0x20;
        private final int payloadCapacityBody = 0xb0 + nonceSize + 0xb0 + 0xa0;

        private final int payloadPrunableAttachmentComponentCapacity = 0xa800;
        private final int payloadPrunableCapacityBody = payloadPrunableAttachmentComponentCapacity + payloadPrunableAttachmentComponentCapacity;

        private final int payloadCapacityExtraTail = nonceSize;
    }

    private static final ApplicationConfiguration applicationConfiguration = new ApplicationConfiguration();

    private static class Configuration {
        private long payloadCountLimit = 1000;

        private List<String> chainString = new ArrayList<>();
        private List<String> feeRateNQTString = new ArrayList<>();

        private String feeRateNQTForAssetString = "0";

        private boolean usePrunableAttachment = false;

        private boolean broadcast = false;

        private RestApi signRestApi = null;
        private String signSessionId = null;
    }

    private static final Configuration configuration = new Configuration();

    private static class Metadata {
        private byte[] privateKey = null;
        private byte[] publicKey = null;
        private boolean verifySignature = false;

        private byte[] magic = new byte[0x04];

        private String fileName = null;

        private boolean remoteSign = false;

        HeaderType headerType = HeaderType.asset;

        String chainIdString = "2";
        String assetIdString = null;
        String aliasString = null;

        String accountPropertySetterString = null;
        String accountPropertyString = "urlData";

        String transactionFullHashString = null;
    }

    private static final Metadata metadata = new Metadata();

    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            applicationResult.put("errorDescription", "usage :\nwa binary configuration\nra assetId configuration\n");
        } else {

            String commandString = args[0];
            String commandParameter = args[1];
            String configurationFileName = "configuration.json";

            if(args.length > 2) {
                configurationFileName = args[2];
            }

            configurationRead(configurationFileName);

            switch(commandString) {
                case "wm": {
                    metadata.headerType = HeaderType.message;
                    assetWriteFiles(commandParameter, metadata.fileName);
                    break;
                }

                case "rm": {
                    try {
                        metadata.headerType = HeaderType.message;

                        List<String> urlPartList = new ArrayList<>();
                        boolean hasChainId = decodeUrl(urlPartList, commandParameter);

                        if(urlPartList.size() >= 2) {
                            metadata.transactionFullHashString = urlPartList.get(1);

                            if(urlPartList.size() > 2) {
                                if(hasChainId) {
                                    metadata.chainIdString = urlPartList.get(2);
                                }
                            }

                            dataRead();
                        } else {
                            applicationResult.put("errorDescription", "invalid url");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        applicationResult.put("errorDescription", "error rebuilding asset data");
                    }
                }

                case "wa": {
                    metadata.headerType = HeaderType.asset;
                    assetWriteFiles(commandParameter, metadata.fileName);
                    break;
                }

                case "ra": {
                    try {
                        metadata.assetIdString = commandParameter;
                        dataRead();

                    } catch (Exception e) {
                        e.printStackTrace();
                        applicationResult.put("errorDescription", "error rebuilding asset data");
                    }
                }

                case "wp": {
                    metadata.headerType = HeaderType.accountProperty;
                    assetWriteFiles(commandParameter, metadata.fileName);
                    break;
                }

                case "rp": {
                    try {
                        metadata.headerType = HeaderType.accountProperty;

                        List<String> urlPartList = new ArrayList<>();
                        boolean hasChainId = decodeUrl(urlPartList, commandParameter);

                        if(urlPartList.size() >= 2) {
                            metadata.aliasString = urlPartList.get(1);

                            if(urlPartList.size() > 2) {
                                int i = 2;

                                if(hasChainId) {
                                    metadata.chainIdString = urlPartList.get(i++);
                                }

                                if(urlPartList.size() > i) {
                                    metadata.accountPropertyString = urlPartList.get(i);
                                }
                            }

                            metadata.accountPropertySetterString = Long.toUnsignedString(ardorApi.accountIdFromAlias(metadata.aliasString, Integer.parseInt(metadata.chainIdString)));

                            dataRead();
                        } else {
                            applicationResult.put("errorDescription", "invalid url");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        applicationResult.put("errorDescription", "error rebuilding asset data");
                    }
                }
            }
        }

        JSONObject applicationResultJson = new JSONObject();

        for(String key : applicationResult.keySet()) {
         applicationResultJson.put(key, applicationResult.get(key));
        }

        System.out.println(applicationResultJson.toJSONString());

        System.exit(0);
    }

    private static void configurationRead(String filePath) throws IOException, ParseException {
        File file;

        try {
            file = new File(filePath);
        } catch (Exception e) {
            applicationResult.put("errorDescription", "could not open file : " + filePath);
            return;
        }

        long fileLength = file.length();

        if (fileLength == 0) {
            applicationResult.put("errorDescription", "empty configuration : " + filePath);
            return;
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonConfiguration = (JSONObject) jsonParser.parse(new String (Files.readAllBytes(file.toPath())));

        metadata.accountPropertyString = JsonSimpleFunctions.getString(jsonConfiguration,"propertyName", metadata.accountPropertyString);

        if(jsonConfiguration.containsKey("privateKey")) {
            nxtCryptography = new NxtCryptography((String) jsonConfiguration.get("privateKey"));
        } else if (jsonConfiguration.containsKey("publicKey")) {
            nxtCryptography = new NxtCryptography((byte[]) null);
            nxtCryptography.setPublicKeyString((String) jsonConfiguration.get("publicKey"));
        }

        if(jsonConfiguration.containsKey("chain")) {
            JSONArray ja = (JSONArray) jsonConfiguration.get("chain");
            configuration.chainString = listStringFromJSONArray(ja);
        } else {
            configuration.chainString.add("2");
        }

        if(jsonConfiguration.containsKey("feeRateNQT")) {
            JSONArray ja = (JSONArray) jsonConfiguration.get("feeRateNQT");
            configuration.feeRateNQTString = listStringFromJSONArray(ja);
        } else {
            configuration.feeRateNQTString.add("0");
        }

        if(configuration.chainString == null || configuration.feeRateNQTString == null || configuration.chainString.size() != configuration.feeRateNQTString.size()) {
            applicationResult.put("errorDescription", "chain does not correspond to feeRateNQT configuration");
        }

        configuration.feeRateNQTForAssetString = JsonSimpleFunctions.getString(jsonConfiguration,"feeRateNQTForAsset", configuration.feeRateNQTForAssetString);
        configuration.usePrunableAttachment = JsonSimpleFunctions.getBoolean(jsonConfiguration,"usePrunableAttachment", configuration.usePrunableAttachment);
        configuration.payloadCountLimit = JsonSimpleFunctions.getLong(jsonConfiguration,"transactionLimit", configuration.payloadCountLimit);
        configuration.broadcast = JsonSimpleFunctions.getBoolean(jsonConfiguration,"broadcast", configuration.broadcast);

        if(jsonConfiguration.containsKey("metadata")) {
            JSONObject jsonMetadata = (JSONObject) jsonConfiguration.get("metadata");

            metadata.privateKey = JsonSimpleFunctions.getBytesFromHexString(jsonMetadata,"privateKey", metadata.privateKey);
            metadata.publicKey = JsonSimpleFunctions.getBytesFromHexString(jsonMetadata,"publicKey", metadata.publicKey);
            metadata.magic = JsonSimpleFunctions.getBytesFromHexString(jsonMetadata,"magic32b", metadata.magic);

            metadata.fileName = JsonSimpleFunctions.getString(jsonMetadata,"fileName", metadata.fileName);
            metadata.verifySignature = JsonSimpleFunctions.getBoolean(jsonMetadata,"verifySignature", metadata.verifySignature);
            metadata.remoteSign = JsonSimpleFunctions.getBoolean(jsonMetadata,"remoteSign", metadata.remoteSign);
        }

        if(jsonConfiguration.containsKey("signHost")) {
            JSONObject jsonSignHost = (JSONObject) jsonConfiguration.get("signHost");

            if(jsonSignHost.containsKey("enabled") && (boolean) jsonSignHost.get("enabled")) {
                String protocol = (String) jsonSignHost.get("protocol");
                String address = (String) jsonSignHost.get("address");
                String port = (String) jsonSignHost.get("port");

                configuration.signRestApi = new RestApi(protocol, address, port);

                configuration.signSessionId = JsonSimpleFunctions.getString(jsonSignHost,"session", configuration.signSessionId);
            }
        }

        ardorApi = new ArdorApi((String) ((JSONObject)jsonConfiguration.get("ardorHost")).get("protocol"), (String) ((JSONObject)jsonConfiguration.get("ardorHost")).get("address"), (String) ((JSONObject)jsonConfiguration.get("ardorHost")).get("port"));
    }

    private static void dataRead() throws IOException {
        JSONObject jsonResponse = null;
        String headerEncoded = null;
        String dataName = "data";

        HashMap<String, String> parameters = new HashMap<>();

        switch (metadata.headerType) {
            case message: {
                parameters.put("requestType", "getTransaction");
                parameters.put("chain", metadata.chainIdString);
                parameters.put("fullHash", metadata.transactionFullHashString);

                jsonResponse = ardorApi.jsonObjectHttpApi(false, parameters);

                if(!jsonResponse.containsKey("attachment")) {
                    applicationResult.put("errorDescription", "header not found");
                    return;
                }

                JSONObject attachment = (JSONObject) jsonResponse.get("attachment");

                if(!attachment.containsKey("message")) {
                    applicationResult.put("errorDescription", "header not found");
                    return;
                }

                headerEncoded = (String) attachment.get("message");
                break;
            }

            case asset: {
                parameters.put("asset", metadata.assetIdString);
                parameters.put("requestType", "getAsset");

                jsonResponse = ardorApi.jsonObjectHttpApi(false, parameters);

                if(!jsonResponse.containsKey("description")) {
                    applicationResult.put("errorDescription", "asset not found");
                    return;
                }

                headerEncoded = (String) jsonResponse.get("description");

                dataName = metadata.assetIdString;

                break;
            }

            case accountProperty: {
                parameters.put("requestType", "getAccountProperties");
                parameters.put("setter", metadata.accountPropertySetterString);
                parameters.put("recipient", metadata.accountPropertySetterString);
                parameters.put("property", metadata.accountPropertyString);

                jsonResponse = ardorApi.jsonObjectHttpApi(false, parameters);

                if(!jsonResponse.containsKey("properties")) {
                    applicationResult.put("errorDescription", "account property not found");
                    return;
                }

                JSONArray properties = (JSONArray) jsonResponse.get("properties");

                if(properties.size() == 0) {
                    applicationResult.put("errorDescription", "account property not found");
                    return;
                }

                JSONObject property = (JSONObject) properties.get(0);
                headerEncoded = (String) property.get("value");
                break;
            }
        }

        if(headerEncoded == null || (headerEncoded.length() != 0x40 && headerEncoded.length() != 0xa0)) {
            applicationResult.put("errorDescription", "header invalid encoded length");
            return;
        }

        byte[] headerDecoded = null;

        try {
            headerDecoded = Base64.getUrlDecoder().decode(headerEncoded);
        } catch (IllegalArgumentException e) { /* empty */ }

        if(headerDecoded == null || (headerDecoded.length != 0x30 && headerDecoded.length != 0x78)) {
            applicationResult.put("errorDescription", "header invalid encoding");
            return;
        }

        applicationResult.put("header", headerEncoded);
        applicationResult.put("headerDecodedBytes", NxtCryptography.hexStringFromBytes(headerDecoded));

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(headerDecoded);

        // format : (magic[0x04] : chain[0x04] : fullHash[0x20] : payloadLength[0x04] : payloadMetadataLength[0x04] : signerAccountId[0x08]) : signature[0x40]

        int bytesRead = 0;

        byte[] magic = new byte[0x04];
        byte[] chainBytes = new byte[0x04];
        byte[] fullHash = new byte[0x20];
        byte[] payloadLengthBytes = new byte[0x04];

        bytesRead += byteArrayInputStream.read(magic, 0x00, 0x04);
        bytesRead += byteArrayInputStream.read(chainBytes, 0x00, 0x04);
        bytesRead += byteArrayInputStream.read(fullHash, 0x00, 0x20);
        bytesRead += byteArrayInputStream.read(payloadLengthBytes, 0x00, 0x04);

        byte[] payloadMetadataLengthBytes = new byte[0x04];

        if(byteArrayInputStream.available() >= 0x04) {
            bytesRead += byteArrayInputStream.read(payloadMetadataLengthBytes, 0x00, 0x04);
        }

        byte[] signerAccountIdBytes = new byte[0x08];
        byte[] signature = new byte[0x40];

        if(byteArrayInputStream.available() == 0x48) {
            bytesRead += byteArrayInputStream.read(signerAccountIdBytes, 0x00, 0x08);
            bytesRead += byteArrayInputStream.read(signature, 0x00, 0x40);

            if(bytesRead != 0x78) {
                applicationResult.put("errorDescription", jsonResponse.toJSONString());
                return;
            }
        } else {
            metadata.verifySignature = false;
        }

        if(metadata.verifySignature)
        {
            long signerAccountId = ByteBuffer.wrap(signerAccountIdBytes).order(ByteOrder.BIG_ENDIAN).getLong();
            byte[] publicKeyForVerification = ardorApi.getAccountPublicKey(signerAccountId);

            byte[] dataForVerification = Arrays.copyOfRange(headerDecoded, 0x00, 0x38);

            boolean isValid = NxtCryptography.verifyBytes(dataForVerification, signature, publicKeyForVerification);

            applicationResult.put("isAssetSignatureValid", Boolean.toString(isValid));

            if(!isValid) {
                applicationResult.put("errorDescription", "invalid signature");
                hashMapAddByteArrayAsHexStringParameter(applicationResult, "publicKey", publicKeyForVerification);
                hashMapAddByteArrayAsHexStringParameter(applicationResult, "data", dataForVerification);
                hashMapAddByteArrayAsHexStringParameter(applicationResult, "signature", signature);

                return;
            }
        }

        int payloadLength = ByteBuffer.wrap(payloadLengthBytes).order(ByteOrder.BIG_ENDIAN).getInt();
        int payloadMetadataLength = ByteBuffer.wrap(payloadMetadataLengthBytes).order(ByteOrder.BIG_ENDIAN).getInt();

        String chainString = Integer.toString(ByteBuffer.wrap(chainBytes).order(ByteOrder.BIG_ENDIAN).getInt());
        String fullHashString = NxtCryptography.hexStringFromBytes(fullHash);

        List<String> listHexString = new ArrayList<>();

        int payloadCounter = 0;

        while(payloadCounter < (payloadLength * 2)) {
            parameters = new HashMap<>();

            parameters.put("chain", chainString);
            parameters.put("fullHash", fullHashString);

            parameters.put("requestType", "getTransaction");

            jsonResponse = ardorApi.jsonObjectHttpApi(false, parameters);

            if(jsonResponse.containsKey("attachment")) {
                JSONObject attachment = (JSONObject) jsonResponse.get("attachment");

                if(!attachment.containsKey("encryptedMessage")) {
                    applicationResult.put("errorDescription", "unable to reassemble payload : missing message data : " + chainString + ":" + fullHashString);
                    return;
                }

                boolean isPrunable =  attachment.containsKey("version.PrunableEncryptedMessage");

                int capacityBody;

                if(isPrunable) {
                    capacityBody = applicationConfiguration.payloadPrunableCapacityBody;

                    if(applicationConfiguration.useChainInterleaveFormatForPrunable) {
                        capacityBody -= 0x04;
                    }

                } else {
                    capacityBody = applicationConfiguration.payloadCapacityBody;
                }

                boolean isTail = ((payloadLength - (payloadCounter / 2 )) <= (capacityBody + applicationConfiguration.payloadCapacityExtraTail));

                if (attachment.containsKey("message")) {
                    String data = (String) attachment.get("message");
                    listHexString.add(0, data);

                    payloadCounter += data.length();
                }

                if (!isPrunable && attachment.containsKey("encryptToSelfMessage")) {
                    JSONObject message = (JSONObject) attachment.get("encryptToSelfMessage");
                    String data = (String) message.get("data");
                    String nonce = (String) message.get("nonce");
                    listHexString.add(0, data);
                    listHexString.add(0, nonce);

                    payloadCounter += data.length() + nonce.length();
                }

                JSONObject message = (JSONObject) attachment.get("encryptedMessage");

                String data;
                String dataPre = (String) message.get("data");
                String nonce = (String) message.get("nonce");

                if(isPrunable && applicationConfiguration.useChainInterleaveFormatForPrunable && !isTail) {
                    String chainStringDataString = dataPre.substring(0x00, 0x08);
                    data = dataPre.substring(0x08);
                    chainString = Integer.toString(ByteBuffer.wrap(NxtCryptography.bytesFromHexString(chainStringDataString)).order(ByteOrder.BIG_ENDIAN).getInt());
                }
                else {
                    data = dataPre;
                }

                listHexString.add(0, data);

                payloadCounter += data.length();

                if (isTail) {
                    listHexString.add(0, nonce);
                    payloadCounter += nonce.length();
                } else {
                    fullHashString = nonce;
                }
            } else {
                applicationResult.put("errorDescription", "missing transaction : " + chainString + ":" + fullHashString);
                return;
            }
        }

        StringBuilder sb = new StringBuilder();

        for(String hexString : listHexString) {
            sb.append(hexString);
        }

        String payloadHexString = sb.toString();
        byte[] payloadDataPadded = NxtCryptography.bytesFromHexString(payloadHexString);

        if(payloadDataPadded.length < payloadLength) {
            applicationResult.put("errorDescription", "missing data : " + payloadDataPadded.length + " : expected : " + payloadLength);
            return;
        }

        if(payloadMetadataLength != 0 && payloadMetadataLength <= payloadLength) {
            byte[] payloadData = Arrays.copyOfRange(payloadDataPadded, 0, payloadMetadataLength);

            String fileName = dataName + ".metadata";
            File file = new File(fileName);
            Files.write(file.toPath(), payloadData);

            applicationResult.put("metadataFileName", fileName);
            applicationResult.put("metadataFileSize", Integer.toString(payloadData.length));
            applicationResult.put("metadataFileHash", NxtCryptography.hexStringFromBytes(messageDigest.digest(payloadData)));
        }

        if(payloadMetadataLength < payloadLength) {
            byte[] payloadData = Arrays.copyOfRange(payloadDataPadded, payloadMetadataLength, payloadLength);

            File file = new File(dataName);
            Files.write(file.toPath(), payloadData);

            applicationResult.put("dataFileName", dataName);
            applicationResult.put("dataFileSize", Integer.toString(payloadData.length));
            applicationResult.put("dataFileHash", NxtCryptography.hexStringFromBytes(messageDigest.digest(payloadData)));
        }
    }

    private static void dataWrite(byte[] payloadData, int metadataLength) throws IOException {

        int payloadDataCounter = 0;

        applicationResult.put("payloadHash", NxtCryptography.hexStringFromBytes(messageDigest.digest(payloadData)));

        List<JSONObject> transactionResponseList = new ArrayList<>();
        List<byte[]> transactionBytesList = new ArrayList<>();

        long feeFQTCumulative = 0;

        while (payloadDataCounter < payloadData.length) {
            payloadDataCounter += addPayloadTransactionToList(transactionResponseList, transactionBytesList, payloadData, payloadDataCounter, payloadData.length);
        }

        if(!addHeaderTransactionToList(transactionResponseList, transactionBytesList, payloadData.length, metadataLength, metadata.privateKey, metadata.magic, metadata.headerType)) {
            return;
        }

        for(JSONObject transactionResponse : transactionResponseList) {
            feeFQTCumulative += ardorApi.transactionResponseCalculateFeeFQT(transactionResponse);
        }

        if(configuration.broadcast) {

            for (int i = 0; i < transactionBytesList.size(); i++) {

                byte[] transactionBytes = transactionBytesList.get(i);
                JSONObject transactionResponseJson = transactionResponseList.get(i);

                String prunableAttachmentJSONString = null;

                if(configuration.usePrunableAttachment) {
                    JSONObject transactionJSON = (JSONObject) transactionResponseJson.get("transactionJSON");

                    if(transactionJSON.containsKey("attachment")) {
                        JSONObject attachment = (JSONObject) transactionJSON.get("attachment");
                        prunableAttachmentJSONString = attachment.toJSONString();
                    }
                }

                ardorApi.transactionBytesBroadcast(transactionBytes, prunableAttachmentJSONString);
            }
        }

        byte[] fullHash = ArdorTransaction.calculateTransactionBytesFullHash(transactionBytesList.get(transactionBytesList.size() - 1));
        long assetId = NxtCryptography.longLsbFromBytes(fullHash);

        applicationResult.put("assetTransactionJSON", ((JSONObject)transactionResponseList.get(transactionResponseList.size() - 1).get("transactionJSON")).toJSONString());
        applicationResult.put("asset", Long.toUnsignedString(assetId));
        applicationResult.put("payloadSize", Integer.toString(payloadData.length));
        applicationResult.put("transactionCount", Integer.toString(transactionResponseList.size()));
        applicationResult.put("feeFQT", Long.toUnsignedString(feeFQTCumulative));
        applicationResult.put("feeFloat", Double.toString(feeFQTCumulative / 100000000.0));
    }

    private static boolean addHeaderTransactionToList(List<JSONObject> transactionResponseList, List<byte[]> transactionBytesList, int payloadLength, int payloadMetadataLength, byte[] privateKeyForMetadataSigning, byte[] magic, HeaderType headerType) throws IOException {

        byte[] metadataBytes = generateMetadataBytes(transactionResponseList, transactionBytesList, payloadLength, payloadMetadataLength, magic, privateKeyForMetadataSigning);

        if(metadataBytes == null) {
            return false;
        }

        String metadataString = Base64.getUrlEncoder().withoutPadding().encodeToString(metadataBytes);

        HashMap<String, String> parameters = new HashMap<>();

        switch (headerType) {
            case message: {
                parameters.put("requestType", "sendMessage");
                parameters.put("chain", configuration.chainString.get(0));
                parameters.put("message",metadataString);
                parameters.put("messageIsText", "true");
                parameters.put("messageIsPrunable", Boolean.toString(configuration.usePrunableAttachment));
                break;
            }

            case asset: {
                parameters.put("requestType", "issueAsset");
                parameters.put("chain", "2");
                parameters.put("description", metadataString);
                parameters.put("name", metadata.accountPropertyString);
                parameters.put("quantityQNT", "1");
                break;
            }

            case accountProperty: {
                parameters.put("requestType", "setAccountProperty");
                parameters.put("chain", "2");
                parameters.put("property", metadata.accountPropertyString);
                parameters.put("value", metadataString);
                break;
            }
        }

        hashMapAddByteArrayAsHexStringParameter(parameters, "publicKey", nxtCryptography.getPublicKey());

        parameters.put("feeRateNQT", configuration.feeRateNQTForAssetString);
        parameters.put("deadline", "1440");

        parameters.put("broadcast", "false");

        JSONObject jsonTransaction = ardorApi.jsonObjectHttpApi(true, parameters);

        if (jsonTransaction.containsKey("errorCode")) {
            applicationResult.put("errorDescription", jsonTransaction.toJSONString());
            return false;
        }

        transactionResponseList.add(jsonTransaction);

        byte[] transactionBytes = NxtCryptography.bytesFromHexString((String) jsonTransaction.get("unsignedTransactionBytes"));

        boolean signValid = signAndAddTransactionToList(transactionBytesList, transactionBytes);

        if(!signValid) {
            return false;
        }

        return true;
    }

    private static byte[] generateMetadataBytes(List<JSONObject> transactionResponseList, List<byte[]> transactionBytesList, int payloadLength, int payloadMetadataLength, byte[] magic, byte[] privateKeyForMetadataSigning) throws IOException {
        JSONObject transactionResponse = transactionResponseList.get(transactionResponseList.size() - 1);

        if(transactionResponse.containsKey("errorCode")) {
            applicationResult.put("errorDescription", transactionResponse.toJSONString());
            return null;
        }

        JSONObject transactionJSON = (JSONObject) transactionResponse.get("transactionJSON");
        byte[] transactionBytes = NxtCryptography.bytesFromHexString((String) transactionResponse.get("unsignedTransactionBytes"));

        boolean signValid = signAndAddTransactionToList(transactionBytesList, transactionBytes);

        if(!signValid) {
            return null;
        }

        long chainId = (long) transactionJSON.get("chain");

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        // format : (magic[0x04] : chain[0x04] : fullHash[0x20] : payloadLength[0x04] : payloadMetadataLength[0x04] : signerAccountId[0x08]) : signature[0x40]

        byteArrayOutputStream.write(magic);

        byteArrayOutputStream.write(ByteBuffer.allocate(0x04).order(ByteOrder.BIG_ENDIAN).putInt((int) chainId).array());
        byteArrayOutputStream.write(ArdorTransaction.calculateTransactionBytesFullHash(transactionBytes));

        byteArrayOutputStream.write(ByteBuffer.allocate(0x04).order(ByteOrder.BIG_ENDIAN).putInt(payloadLength).array());
        byteArrayOutputStream.write(ByteBuffer.allocate(0x04).order(ByteOrder.BIG_ENDIAN).putInt(payloadMetadataLength).array());

        if(metadata.remoteSign || privateKeyForMetadataSigning != null)
        {
            System.out.println("signing " + metadata.remoteSign + " : " + privateKeyForMetadataSigning + " : " + metadata.privateKey);
            // HMAC would be more efficient but DSA allows 3rd party verification

            byte[] signature = new byte[0x40];

            if (privateKeyForMetadataSigning != null) {

                NxtCryptography.signBytes(byteArrayOutputStream.toByteArray(), signature, 0, privateKeyForMetadataSigning);

                hashMapAddByteArrayAsHexStringParameter(applicationResult, "description.publicKey", NxtCryptography.publicKeyFromPrivateKey(privateKeyForMetadataSigning));
                hashMapAddByteArrayAsHexStringParameter(applicationResult, "description.data", byteArrayOutputStream.toByteArray());
                hashMapAddByteArrayAsHexStringParameter(applicationResult, "description.signature", signature);

            } else {
                // TODO optional request sign from remote service
                applicationResult.put("errorDescription", "metadata remote sign not implemented");
                return null;
            }

            byteArrayOutputStream.write(ByteBuffer.allocate(0x08).order(ByteOrder.BIG_ENDIAN).putLong(NxtCryptography.accountIdFromPrivateKey(privateKeyForMetadataSigning)).array()); // TODO replace with explicit account
            byteArrayOutputStream.write(signature);
        }

        JSONObject assetMetadata = new JSONObject();

        hashMapAddByteArrayAsHexStringParameter(assetMetadata, "fullHash", ArdorTransaction.calculateTransactionBytesFullHash(transactionBytes));

        assetMetadata.put("length", payloadLength);
        assetMetadata.put("chain", configuration.chainString);

        return byteArrayOutputStream.toByteArray();
    }

    private static int addPayloadTransactionToList(List<JSONObject> transactionResponseList, List<byte[]> transactionBytesList, byte[] payloadData, int dataOffset, int limit) throws IOException {
        int payloadSizePadded = 0;

        byte[] bytesMENonce;
        byte[] bytesME = new byte[1];

        byte[] bytesMESNonce = null;
        byte[] bytesMES = new byte[1];

        byte[] bytesM = null;

        if (dataOffset >= limit) {
            return 0;
        }

        if (transactionResponseList.size() == 0) {
            bytesMENonce = getBytesPadded(payloadData, dataOffset + payloadSizePadded, applicationConfiguration.payloadCapacityExtraTail, (byte) 0x00);
            payloadSizePadded += applicationConfiguration.payloadCapacityExtraTail;
        } else {
            JSONObject transactionResponse = transactionResponseList.get(transactionResponseList.size() - 1);

            if(transactionResponse.containsKey("errorCode")) {
                System.out.println(transactionResponse.toJSONString());
                System.exit(8);
            }

            byte[] transactionBytes = NxtCryptography.bytesFromHexString((String) transactionResponse.get("unsignedTransactionBytes"));

            signAndAddTransactionToList(transactionBytesList, transactionBytes);

            bytesMENonce = ArdorTransaction.calculateTransactionBytesFullHash(transactionBytes);
        }

        if((dataOffset + payloadSizePadded) < limit) {
            int messageSize = limit - (dataOffset + payloadSizePadded);

            if(!configuration.usePrunableAttachment) {
                if (messageSize > 0xa0 + 0x10) {
                    messageSize = 0xa0 + 0x10;
                }
            } else {
                int payloadCapacityReductionFromChainParameter = 0x00;

                if(applicationConfiguration.useChainInterleaveFormatForPrunable) {
                    payloadCapacityReductionFromChainParameter = 0x04;
                }

                if (messageSize > applicationConfiguration.payloadPrunableAttachmentComponentCapacity - payloadCapacityReductionFromChainParameter) {
                    messageSize = applicationConfiguration.payloadPrunableAttachmentComponentCapacity - payloadCapacityReductionFromChainParameter;
                }
            }

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            if(configuration.usePrunableAttachment && applicationConfiguration.useChainInterleaveFormatForPrunable && transactionResponseList.size() != 0) {

                JSONObject transactionResponse = transactionResponseList.get(transactionResponseList.size() - 1);
                JSONObject transactionJSON = (JSONObject) transactionResponse.get("transactionJSON");

                byteArrayOutputStream.write(ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(Integer.parseUnsignedInt(Long.toUnsignedString((Long) transactionJSON.get("chain")))).array());
            }

            byteArrayOutputStream.write(Arrays.copyOfRange(payloadData, dataOffset + payloadSizePadded, dataOffset + payloadSizePadded + messageSize));

            bytesME = byteArrayOutputStream.toByteArray();

            payloadSizePadded += messageSize;
        }

        if(!configuration.usePrunableAttachment) {
            if ((dataOffset + payloadSizePadded) < limit) {
                bytesMESNonce = getBytesPadded(payloadData, dataOffset + payloadSizePadded, applicationConfiguration.nonceSize, (byte) 0x00);
                payloadSizePadded += applicationConfiguration.nonceSize;
            }

            if((dataOffset + payloadSizePadded) < limit) {
                int messageSize = limit - (dataOffset + payloadSizePadded);

                if (messageSize > 0xa0 + 0x10) {
                    messageSize = 0xa0 + 0x10;
                }

                bytesMES = Arrays.copyOfRange(payloadData, dataOffset + payloadSizePadded, dataOffset + payloadSizePadded + messageSize);
                payloadSizePadded += messageSize;
            }
        }

        if((dataOffset + payloadSizePadded) < limit) {
            int messageSize = limit - (dataOffset + payloadSizePadded);

            if(!configuration.usePrunableAttachment) {
                if (messageSize > 0xa0) {
                    messageSize = 0xa0;
                }
            } else {
                if (messageSize > applicationConfiguration.payloadPrunableAttachmentComponentCapacity) {
                    messageSize = applicationConfiguration.payloadPrunableAttachmentComponentCapacity;
                }
            }

            bytesM = Arrays.copyOfRange(payloadData, dataOffset + payloadSizePadded, dataOffset + payloadSizePadded + messageSize);
            payloadSizePadded += messageSize;
        }

        HashMap<String, String> parameters = new HashMap<>();

        if(bytesMENonce == null) {
            throw new NullPointerException();
        }

        hashMapAddByteArrayAsHexStringParameter(parameters, "encryptedMessageNonce", bytesMENonce);
        hashMapAddByteArrayAsHexStringParameter(parameters, "encryptedMessageData", bytesME);

        parameters.put("messageToEncryptIsText", "false");
        parameters.put("compressMessageToEncrypt", "false");

        parameters.put("encryptedMessageIsPrunable", Boolean.toString(configuration.usePrunableAttachment));

        if(bytesMESNonce != null) {
            hashMapAddByteArrayAsHexStringParameter(parameters, "encryptToSelfMessageNonce", bytesMESNonce);
            hashMapAddByteArrayAsHexStringParameter(parameters, "encryptToSelfMessageData", bytesMES);

            parameters.put("messageToEncryptToSelfIsText", "false");
            parameters.put("compressMessageToEncryptToSelf", "false");
        }

        if(bytesM != null) {
            hashMapAddByteArrayAsHexStringParameter(parameters, "message", bytesM);

            parameters.put("messageIsText", "false");
            parameters.put("messageIsCompressed", "false");
            parameters.put("messageIsPrunable", Boolean.toString(configuration.usePrunableAttachment));
        }

        parameters.put("publicKey", nxtCryptography.getPublicKeyString());

        parameters.put("chain", configuration.chainString.get(indexForInterleave));
        parameters.put("feeRateNQTPerFXT", configuration.feeRateNQTString.get(indexForInterleave));

        if(configuration.usePrunableAttachment && applicationConfiguration.useChainInterleaveFormatForPrunable) {
            indexForInterleave++;

            if(indexForInterleave >= configuration.chainString.size()) {
                indexForInterleave = 0;
            }
        }

        parameters.put("deadline", "1440");
        parameters.put("recipient", nxtCryptography.getAccountIdString());

        parameters.put("broadcast", "false");

        parameters.put("requestType", "sendMessage");

        JSONObject jsonTransaction = ardorApi.jsonObjectHttpApi(true, parameters);
        transactionResponseList.add(jsonTransaction);

        if(jsonTransaction.containsKey("errorCode")) {
            System.out.println(jsonTransaction.toJSONString());
            System.exit(1);
        }

        return payloadSizePadded;
    }

    private static boolean signAndAddTransactionToList(List<byte[]> transactionBytesList, byte[] transactionBytes) throws IOException {

        if(configuration.signRestApi == null) {
            ArdorTransaction.signUnsignedTransactionBytes(transactionBytes, nxtCryptography.getPrivateKey());
        } else {
            HashMap<String, String> parametersForSignRequest = new HashMap<>();

            parametersForSignRequest.put("requestType", "signTransaction");

            hashMapAddByteArrayAsHexStringParameter(parametersForSignRequest,"unsignedTransactionBytes", transactionBytes);

            if(configuration.signSessionId != null) {
                parametersForSignRequest.put("session", configuration.signSessionId);
            }

            JSONObject response = configuration.signRestApi.jsonObjectHttpApi(true, parametersForSignRequest);

            if(!response.containsKey("errorDescription") && response.containsKey("transactionBytes")) {
                transactionBytes = NxtCryptography.bytesFromHexString((String) response.get("transactionBytes"));
            } else {
                applicationResult.put("errorDescription", response.toJSONString());
                return false;
            }
        }

        transactionBytesList.add(transactionBytes);

        return true;
    }

    private static void assetWriteFiles(String dataFilePath, String metadataFilePath) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        long metadataFileLength = 0;

        if(metadataFilePath != null) {

            metadataFileLength = writeFileToByteArrayOutputStream(baos, metadataFilePath);

            if(metadataFileLength == 0) {
                return;
            }

            applicationResult.put("metadataFileSize", Long.toUnsignedString(metadataFileLength));
        }

        long dataFileLength;

        if(dataFilePath != null) {

            dataFileLength = writeFileToByteArrayOutputStream(baos, dataFilePath);

            if(dataFileLength == 0) {
                return;
            }

            applicationResult.put("dataFileSize", Long.toUnsignedString(dataFileLength));
        }

        byte[] payloadData = baos.toByteArray();

        long payloadTransactionCount = payloadData.length / applicationConfiguration.payloadCapacityBody;
        long payloadTail = payloadData.length % applicationConfiguration.payloadCapacityBody;

        if (payloadTail > applicationConfiguration.payloadCapacityExtraTail) {
            payloadTransactionCount++;
        }

        if (payloadTransactionCount > configuration.payloadCountLimit) {
            applicationResult.put("errorDescription", "required transactions (" + payloadTransactionCount + ") exceeds configured limit of " + configuration.payloadCountLimit);
            return;
        }

        dataWrite(payloadData, (int) metadataFileLength);
    }

    private static long writeFileToByteArrayOutputStream(ByteArrayOutputStream baos, String filePath) throws IOException {
        long fileLength = 0;

        if(filePath != null) {
            File dataFile;

            try {
                dataFile = new File(filePath);
            } catch (Exception e) {
                applicationResult.put("errorDescription", "could not open file : " + filePath);
                return fileLength;
            }

            fileLength = dataFile.length();

            if (fileLength == 0) {
                applicationResult.put("errorDescription", "empty file : " + dataFile);
                return fileLength;
            }

            baos.write(Files.readAllBytes(dataFile.toPath()));
        }

        return fileLength;
    }

    private static void hashMapAddByteArrayAsHexStringParameter(HashMap<String, String> hashMap, String keyString, byte[] byteParameter) {
        hashMap.put(keyString, String.format("%0" + (byteParameter.length << 1) + "x", new BigInteger(1, byteParameter)));
    }

    private static byte[] getBytesPadded(byte[] data, int dataOffset, int paddedSize, byte padByte) throws IOException {

        if (dataOffset >= data.length) {
            return null;
        }

        byte[] bytesPadded;

        int padSize = 0;
        int dataSizeRemaining = data.length - dataOffset;

        if (dataSizeRemaining < paddedSize) {
            padSize = paddedSize - dataSizeRemaining;
        }

        byte[] bytesFromData;

        if (padSize == 0) {
            bytesFromData = Arrays.copyOfRange(data, dataOffset, dataOffset + paddedSize);
        } else {
            bytesFromData = Arrays.copyOfRange(data, dataOffset, data.length);
        }

        ByteArrayOutputStream boas = new ByteArrayOutputStream();

        boas.write(bytesFromData);

        for (int i = 0; i < padSize; i++) {
            boas.write(padByte);
        }

        bytesPadded = boas.toByteArray();

        return bytesPadded;
    }

    private static boolean decodeUrl(List<String> urlPartList, String urlString) {
        boolean hasChainId = false;

        String[] chainParts = urlString.split(":");

        if(chainParts.length > 2) {
            hasChainId = true;
        }

        String[] parts = urlString.replaceAll("//","").replaceAll("/",":").split(":");

        for(String part : parts) {
            urlPartList.add(part);
        }

        return hasChainId;
    }

    static public List<String> listStringFromJSONArray(JSONArray jsonArray) {

        int jsonArraySize = jsonArray.size();

        if( jsonArraySize == 0)
            return null;

        List<String> list = new ArrayList<>();

        for (Object o : jsonArray) {
            list.add((String) o);
        }

        return list;
    }
}
