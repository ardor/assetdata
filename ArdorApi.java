import org.json.simple.JSONObject;

import java.io.IOException;
import java.math.BigInteger;
import java.util.HashMap;

public class ArdorApi extends RestApi{

    ArdorApi(String hostProtocolString, String hostNameString, String hostPortString) {
        super(hostProtocolString, hostNameString, hostPortString);
    }

    public byte[] getAccountPublicKey(long accountId) throws IOException {
        byte[] publicKey = null;

        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("account", Long.toUnsignedString(accountId));

        parameters.put("requestType", "getAccount");
        JSONObject response = jsonObjectHttpApi(true, parameters);

        if(response.containsKey("publicKey")) {
            publicKey = NxtCryptography.bytesFromHexString((String) response.get("publicKey"));
        }
        else {
            System.out.println(response.toJSONString());
        }

        return publicKey;
    }

    public void transactionBytesBroadcast(byte[] transactionBytes, String prunableAttachmentJSONString) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        hashMapAddByteArrayAsHexStringParameter(parameters, "transactionBytes", transactionBytes);

        if(prunableAttachmentJSONString != null) {
            parameters.put("prunableAttachmentJSON", prunableAttachmentJSONString);
        }

        parameters.put("requestType", "broadcastTransaction");
        JSONObject response = jsonObjectHttpApi(true, parameters);

        if(response.containsKey("errorCode")) {
            System.out.println(response.toJSONString());
            System.exit(1);
        }
    }

    public long transactionResponseCalculateFeeFQT(JSONObject jsonTransactionResponse) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("transactionJSON", ((JSONObject) jsonTransactionResponse.get("transactionJSON")).toJSONString());

        parameters.put("requestType", "calculateFee");
        JSONObject response = jsonObjectHttpApi(true, parameters);

        if(!response.containsKey("feeNQT")) {
            System.out.println(response.toJSONString());
            System.exit(1);
        }

        return Long.parseUnsignedLong((String) response.get("feeNQT"));
    }

    public long accountIdFromAlias(String aliasString, int chain) throws IOException {
        HashMap<String, String> parameters = new HashMap<>();

        parameters.put("aliasName", aliasString);
        parameters.put("chain", Integer.toString(chain));

        parameters.put("requestType", "getAlias");
        JSONObject response = jsonObjectHttpApi(true, parameters);

        if(!response.containsKey("account")) {
            System.out.println(response.toJSONString());
            System.exit(1);
        }

        return Long.parseUnsignedLong((String) response.get("account"));
    }

    private static void hashMapAddByteArrayAsHexStringParameter(HashMap<String, String> hashMap, String keyString, byte[] byteParameter) {
        hashMap.put(keyString, String.format("%0" + (byteParameter.length << 1) + "x", new BigInteger(1, byteParameter)));
    }
}
